from tkinter import Tk, Toplevel, filedialog, StringVar, LabelFrame
from tkinter.ttk import Frame, Button, Label, Combobox
from tkinter import BOTH, TOP, W, LEFT, N, X, E, RIGHT

from typing import List

from analysis_result import AnalyseResultFrame
from params import PARAMS

import docx


class ParamsInputFrame(Frame):
    def __init__(self, parent: Tk or Toplevel, param_ids: List[int]):
        Frame.__init__(self, parent)
        self.parent = parent
        self.param_ids = param_ids
        self.paragraph_params = [param_id for param_id in param_ids if not PARAMS[param_id].is_run]
        self.run_params = [param_id for param_id in param_ids if PARAMS[param_id].is_run]

        self.pack(fill=BOTH, expand=1)
        self.centerWindow()

        self.parent.title('Введите параметры для анализа')

        self.user_input = {param_id: StringVar() for param_id in self.param_ids}

        main_container = Frame(self)
        main_container.pack(fill=BOTH)


        self.left_container = Frame(main_container)
        self.right_container = AnalyseResultFrame(main_container, title="Параметры в тексте")

        left_lbl = Label(self.left_container, text='Параметры пользователя')
        left_lbl.pack(side=TOP)
        right_lbl = Label(self.right_container, text='Параметры в тексте')
        right_lbl.pack(side=RIGHT)

        self.left_container.pack(side=LEFT, anchor=N)
        self.right_container.pack(side=LEFT)


        params_container = Frame(self.left_container)

        __params = list(PARAMS.values())
        p = {
            'Поля': __params[:4],
            'Шрифт': __params[4:9],
            'Абзац': __params[9:],
        }

        for category_name, params in p.items():
            group = LabelFrame(params_container, text=category_name)
            group.pack(side=TOP, fill=X)


            for param in params:
                param_id = 0
                for id_, _parameter in PARAMS.items():
                    if param == _parameter:
                        param_id = id_
                inner_container = Frame(group)
                lbl = Label(inner_container, text=param.name)
                text_field = Combobox(inner_container,
                                      textvariable=self.user_input[param_id])
                text_field['values'] = param.default_vals
                text_field.current(param.start_val)

                lbl.pack(side=LEFT, anchor=W)
                text_field.pack(side=LEFT, expand=True, anchor=E)

                inner_container.pack(side=TOP, fill=X, padx=10)


        self.addButton = Button(self.left_container,
                                text="Начать анализ",
                                command=self.new_test)


        params_container.pack(side=TOP, fill=BOTH)
        self.addButton.pack(side=TOP, pady=8)


    def centerWindow(self):
        w = 900
        h = 580
        sw = self.parent.winfo_screenwidth()
        sh = self.parent.winfo_screenheight()
        x = (sw - w) / 2
        y = (sh - h) / 2
        self.parent.geometry('%dx%d+%d+%d' % (w, h, x, y))

    def new_test(self):
        files = filedialog.askopenfilenames()
        if not files:
            return

        result_string = ""
        for path in files:
            doc = docx.Document(path)
            print("Working with file", path)
            result_string += f"Файл {path}\n \nСтиль документа: {doc.styles}\n "

            if round(float(self.user_input[0].get().strip()), 1) != round(doc.sections[0].left_margin.cm, 1):
                result_string += f"\nПоле слева (см): {round(doc.sections[0].left_margin.cm, 1)}\n"
            if round(float(self.user_input[1].get().strip()), 1) != round(doc.sections[0].right_margin.cm, 1):
                result_string += f"\nПоле справа (см): {round(doc.sections[0].right_margin.cm, 1)}\n"
            if round(float(self.user_input[2].get().strip()), 1) != round(doc.sections[0].top_margin.cm, 1):
                result_string += f"\nПоле сверху (см): {round(doc.sections[0].bottom_margin.cm, 1)}\n"
            if round(float(self.user_input[3].get().strip()), 1) != round(doc.sections[0].bottom_margin.cm, 1):
                result_string += f"\nПоле снизу (см): {round(doc.sections[0].top_margin.cm, 1)}\n"

            for paragraph_ind, paragraph in enumerate(doc.paragraphs):
                formatting = paragraph.paragraph_format
                par_string = ""
                for param_id in self.paragraph_params:
                    if param_id > 3:
                        param = PARAMS[param_id]
                        user_input_val = self.user_input[param_id].get()
                        in_doc_val = str(param.getter(formatting))
                        if user_input_val.strip() != in_doc_val.strip():
                            par_string += f"   {param.name}: {in_doc_val}\n"

                if not self.run_params:
                    continue

                for run_id, run in enumerate(paragraph.runs):
                    run_string = ""
                    for param_id in self.run_params:
                        if param_id > 3:
                            param = PARAMS[param_id]
                            user_input_val = self.user_input[param_id].get()
                            in_doc_val = str(param.getter(run))
                            if user_input_val.strip() != in_doc_val.strip():
                                run_string += f"      {param.name}: {in_doc_val}\n"
                            if run_string:
                                par_string += f"   Run {run_id + 1}\n{run_string}"

                if par_string:
                    style = paragraph.style
                    result_string += f"\nАбзац {paragraph_ind + 1}\n{style}\n{par_string}\n "

        self.right_container.set_text(result_string)


def main():
    from params import PARAMS

    root = Tk()
    ParamsInputFrame(root, param_ids=list(PARAMS.keys()))
    root.mainloop()


if __name__ == '__main__':
    main()

